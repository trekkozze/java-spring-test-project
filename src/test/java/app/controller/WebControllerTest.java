package app.controller;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.util.Assert;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class WebControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void contexLoads() throws Exception {
		Assert.isTrue(restTemplate!=null, "restTemplate is null!");
	}
	
	


}
