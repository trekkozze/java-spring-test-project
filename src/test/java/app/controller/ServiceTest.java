package app.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import app.bean.Book;
import app.controller.RestServiceClient;
import app.exceptions.ServiceException;

public class ServiceTest {
	
	@Test
	public void testGetObject() throws ServiceException {
		ResponseEntity<Book> book= new RestServiceClient<Book>().findEntity("http://jsonplaceholder.typicode.com/todos/1", Book.class);
		Assert.isTrue(book!=null,"book is empty");
	}

}
