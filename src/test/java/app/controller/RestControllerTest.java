package app.controller;

import java.util.logging.Logger;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.util.Assert;

import app.bean.Book;
import app.bean.Note;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class RestControllerTest {
	
	 private static final Logger log = Logger.getLogger(RestControllerTest.class.getName());


	@Autowired
	private TestRestTemplate restTemplate;
	

	@Test
	public void bookTest() throws Exception {
		
		Book b= this.restTemplate.getForObject("/restNotAuth_book", Book.class) ; 
		
		Assert.isTrue(b.getId()!=null,"body is null");
		
		log.info("id:"+b.getId());
		
	}
	
	
	@Test
	public void notesTest() {
		
		Note[] notes =this.restTemplate.getForObject("/restNotAuth_notes", Note[].class) ; 
		
		Assert.isTrue(notes[0].getId()!=null,"id is null");
		
		log.info("comments:"+notes.toString());
		
	}
}