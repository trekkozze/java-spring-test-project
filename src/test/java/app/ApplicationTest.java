package app;

import java.util.logging.Logger;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import app.controller.RestControllerTest;


@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ApplicationTest {
	
	 private static final Logger log = Logger.getLogger(RestControllerTest.class.getName());
	@Test
	public void startApplication() {
		log.info("application start succesfully!");
	}

}
