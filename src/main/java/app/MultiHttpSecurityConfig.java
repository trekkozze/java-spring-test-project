package app;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class MultiHttpSecurityConfig {
 
@Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {  
	
	
      auth.inMemoryAuthentication()
      	.withUser("pippo").password("{noop}password").roles("USER")
      	.and()
      	.withUser("pluto").password("{noop}password").roles("USER");
  }

  @Configuration
  public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

      @Override
      protected void configure(HttpSecurity http) throws Exception {
		http.
          authorizeRequests()
             .antMatchers("/", "/home").permitAll()
             .antMatchers("/css/**","/js/**").permitAll()
             .antMatchers(HttpMethod.GET, "/rest/**").permitAll()
             .antMatchers(HttpMethod.GET, "/clickB1*").permitAll()
             .antMatchers(HttpMethod.GET, "/restNotAuth_book","/restNotAuth_notes").permitAll()
             .anyRequest().authenticated()
             .and()
          .servletApi().rolePrefix("")
          	 .and()
          .formLogin()
             .loginPage("/login")
             .permitAll()
             .and()
             .logout()
             .permitAll()
             .and()
             .authorizeRequests();
      }
  }
}