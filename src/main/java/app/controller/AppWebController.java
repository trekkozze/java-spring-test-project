package app.controller;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import app.bean.Book;
import app.bean.Note;
import app.exceptions.WebControllerException;

@Controller
public class AppWebController {
	
	 private static final Logger log = Logger.getLogger(AppWebController.class.getName());



	@GetMapping("/clickB1")
	public String loadUserNotes(@RequestParam String userId, Model model, HttpServletRequest request) throws WebControllerException {

		Note[] notes;
		try {
			notes = new RestTemplate().getForObject(getBaseURL(request)+"/rest/notesByUserId?id=" + userId,
					Note[].class);
		} catch (RestClientException e) {
			log.severe("loadUserNotes error:"+e.getMessage());
			throw new WebControllerException ("loadUserNotes error:"+e.getMessage(),e);
		}

		model.addAttribute("notes", notes);
		
		model.addAttribute("userId", userId);

		return "privatePage";

	}
	
	@GetMapping("/clickB2")
	public String loadBook(HttpServletRequest request,@RequestParam String userId, Model model) throws WebControllerException {
		Book book;
		try {
			book = new RestTemplate().getForObject(getBaseURL(request)+"/rest/bookByUserId?id=" + userId,
					Book.class);
		} catch (RestClientException e) {
			log.severe("loadBook error:"+e.getMessage());
			throw new WebControllerException ("loadBook error:"+e.getMessage(),e);
		}

		model.addAttribute("book", book);
		
		model.addAttribute("userId", userId);

		return "privatePage";

	}

	@GetMapping("/privatePage")
	public void loadUserID(HttpServletRequest request, Model model) throws WebControllerException {
		
		String user = request.getRemoteUser();
		String id;
		try {
			id = new RestTemplate().getForObject(getBaseURL(request)+"/rest/idByUserName?user=" + user,
					String.class);
		} catch (RestClientException e) {
			log.severe("privatePage error during userId evaluation:"+e.getMessage());
			throw new WebControllerException ("privatePage error during userId evaluation:"+e.getMessage(),e);
		}
		model.addAttribute("userId", id);
	}
	
	private static String getBaseURL(HttpServletRequest request) {
		String host = request.getServerName();
		int port = request.getServerPort();
		String appName = request.getContextPath();
		return "http://"+host+":"+port+"/"+appName;
	}
}
