package app.controller;

import java.util.logging.Logger;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import app.bean.Book;
import app.bean.Note;

@RestController
@EnableAutoConfiguration
public class AppRestController {

	 private static final Logger log = Logger.getLogger(AppRestController.class.getName());

	@RequestMapping("/restNotAuth_book")
	ResponseEntity<Book> bookNotAuth() {
		log.info("called restNotAuth_book");
		return new RestServiceClient<Book>().findEntity("http://jsonplaceholder.typicode.com/todos/1", Book.class);
	}

	@RequestMapping("/restNotAuth_notes")
	ResponseEntity<Note[]> commentsNotAuth() {
		log.info("called restNotAuth_notes");
		return new RestServiceClient<Note[]>().findEntity("https://jsonplaceholder.typicode.com/posts/1/comments", Note[].class);

	}

	@RequestMapping("/rest/bookByUserId")
	ResponseEntity<Book> getBookById(@RequestParam String id) {
		log.info("called bookByUserId");
		return new RestServiceClient<Book>().findEntity("http://jsonplaceholder.typicode.com/todos/" + id, Book.class);

	}

	@RequestMapping("/rest/notesByUserId")
	ResponseEntity<Note[]> getNotes(@RequestParam String id) {
		log.info("called notesByUserId");
		return new RestServiceClient<Note[]>().findEntity("https://jsonplaceholder.typicode.com/posts/" + id + "/comments",
				Note[].class);
	}

	@RequestMapping("/rest/idByUserName")
	String getUserID(@RequestParam String user) {
		return user.equalsIgnoreCase("pippo") ? "1" : user.equalsIgnoreCase("pluto") ? "2" : "0";
	}

}