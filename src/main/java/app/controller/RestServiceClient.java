package app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Component
public class RestServiceClient<T> {

	@Transactional
	public ResponseEntity<T> findEntity(String uri, Class<T> c) {
		try {
			return new RestTemplate().getForEntity(uri, c, HttpStatus.OK);
		} catch (RestClientException e) {
			return new ResponseEntity<T>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	


}
